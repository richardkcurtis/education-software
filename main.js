$( document ).ready(function() {

$('#chemistry-formula').keypress(function (e) {
    var letter_regex = new RegExp("^[a-zA-Z()]+$");
    var num_regex = new RegExp("^[0-9]+$");
    var char = (!e.charCode ? e.which : e.charCode);
    var str = String.fromCharCode(char);
    if (letter_regex.test(str)) {
        return true;
    }
    else if (num_regex.test(str)) {
        str = String.fromCharCode(char+8272);
        var index = e.target.selectionStart;
        var input = $(this).val();
        $(this).val(input.substr(0,index)+str+input.substr(index));
        $("#chemistry-formula").focus();
        $("#chemistry-formula")[0].setSelectionRange(index+1,index+1);
        return false;
    }
    e.preventDefault();
    return false;
});

$('#go-button').click(function(){
  getMolarMass();
});

var elements = new Array();
elements['H'] = 1.008;
elements['He'] = 4.003;
elements['Li'] = 6.94;
elements['Be'] = 9.012;
elements['B'] = 10.81;
elements['C'] = 12.011;
elements['N'] = 14.007;
elements['O'] = 15.999;
elements['F'] = 18.998;
elements['Ne'] = 20.18;
elements['Na'] = 22.99;
elements['Mg'] = 24.305;
elements['Al'] = 26.982;
elements['Si'] = 28.085;
elements['P'] = 30.974;
elements['S'] = 32.06;
elements['Cl'] = 35.45;
elements['Ar'] = 39.948;
elements['K'] = 39.098;
elements['Ca'] = 40.078;
elements['Sc'] = 44.956;
elements['Ti'] = 47.867;
elements['V'] = 50.942;
elements['Cr'] = 51.996;
elements['Mn'] = 54.938;
elements['Fe'] = 55.845;
elements['Co'] = 58.933;
elements['Ni'] = 58.693;
elements['Cu'] = 63.546;
elements['Zn'] = 65.38;
elements['Ga'] = 69.723;
elements['Ge'] = 72.63;
elements['As'] = 74.922;
elements['Se'] = 78.97;
elements['Br'] = 79.904;
elements['Kr'] = 83.798;
elements['Rb'] = 85.468;
elements['Sr'] = 87.62;
elements['Y'] = 88.906;
elements['Zr'] = 91.224;
elements['Nb'] = 92.906;
elements['Mo'] = 95.95;
elements['Tc'] = 97;
elements['Ru'] = 101.07;
elements['Rh'] = 102.906;
elements['Pd'] = 106.42;
elements['Ag'] = 107.868;
elements['Cd'] = 112.414;
elements['In'] = 114.818;
elements['Sn'] = 118.71;
elements['Sb'] = 121.76;
elements['Te'] = 127.6;
elements['I'] = 126.904;
elements['Xe'] = 131.293;
elements['Cs'] = 132.905;
elements['Ba'] = 137.327;
elements['La'] = 138.905;
elements['Ce'] = 140.116;
elements['Pr'] = 140.908;
elements['Nd'] = 144.242;
elements['Pm'] = 145;
elements['Sm'] = 150.36;
elements['Eu'] = 151.964;
elements['Gd'] = 157.25;
elements['Tb'] = 158.925;
elements['Dy'] = 162.5;
elements['Ho'] = 164.93;
elements['Er'] = 167.259;
elements['Tm'] = 168.934;
elements['Yb'] = 173.045;
elements['Lu'] = 174.967;
elements['Hf'] = 178.49;
elements['Ta'] = 180.948;
elements['W'] = 183.84;
elements['Re'] = 186.207;
elements['Os'] = 190.23;
elements['Ir'] = 192.217;
elements['Pt'] = 195.084;
elements['Au'] = 196.967;
elements['Hg'] = 200.592;
elements['Tl'] = 204.38;
elements['Pb'] = 207.2;
elements['Bi'] = 208.98;
elements['Po'] = 209;
elements['At'] = 210;
elements['Rn'] = 222;
elements['Fr'] = 223;
elements['Ra'] = 226;
elements['Ac'] = 227;
elements['Th'] = 232.038;
elements['Pa'] = 231.036;
elements['U'] = 238.029;
elements['Np'] = 237;
elements['Pu'] = 244;
elements['Am'] = 243;
elements['Cm'] = 247;
elements['Bk'] = 247;
elements['Cf'] = 251;
elements['Es'] = 252;
elements['Fm'] = 257;
elements['Md'] = 258;
elements['No'] = 259;
elements['Lr'] = 262;
elements['Rf'] = 267;
elements['Db'] = 270;
elements['Sg'] = 269;
elements['Bh'] = 270;
elements['Hs'] = 270;
elements['Mt'] = 278;
elements['Ds'] = 281;
elements['Rg'] = 281;
elements['Cn'] = 285;
elements['Nh'] = 286;
elements['Fl'] = 289;
elements['Mc'] = 289;
elements['Lv'] = 293;
elements['Ts'] = 293;
elements['Og'] = 294;

function getMolarMass() {
  var parentheses = new Array();
  var values = new Array();
  var elementList = new Array();
  var elementPos = new Array();
  var elementCount = new Array();
  var elementMass = new Array();
  var chemical = $('#chemistry-formula').val();
  for(var x=0;x<chemical.length;x++) {
    if(chemical[x] == "(" || chemical[x] == ")") {
      parentheses.push(chemical[x]);
    }
    else if(chemical.charCodeAt(x) >= 97 && chemical.charCodeAt(x) <= 122) {
      elementList[elementList.length-1] += chemical[x];
    }
    else if(chemical.charCodeAt(x) >= 65 && chemical.charCodeAt(x) <= 90) {
      elementList.push(chemical[x]);
      elementPos.push(x);
      elementCount.push(1);
      if(chemical.charCodeAt(x+1) >= 97 && chemical.charCodeAt(x+1) <= 122)
        elementMass.push(elements[chemical[x]+chemical[x+1]]);
      else
        elementMass.push(elements[chemical[x]]);
    }
  }
  
  var end = chemical.length-1;
  var pointer = end;
  while(pointer >= 0) {
    var finder = 0;
    var c = 0;
    var num = "";
    while(chemical.charCodeAt(pointer-finder)>=8320 && chemical.charCodeAt(pointer-finder)<=8329) {
      c = chemical.charCodeAt(pointer-finder);
      num = String.fromCharCode(c-8272) + num;
      finder++;
    }
    if(num != "") {
      num = parseInt(num);
      if(chemical.charCodeAt(pointer-finder)==41) {
        var scannerEnd = pointer-finder-1;
        var scannerStart = 0;
        var scanner = scannerEnd;
        var pCount = 1;
        while(pCount != 0) {
          if(chemical.charCodeAt(scanner)==40)
            pCount--;
          else if(chemical.charCodeAt(scanner)==41)
            pCount++;
          if(pCount == 0)
            scannerStart = scanner;
          scanner--;
        }
        for(var y=0;y<elementPos.length;y++) {
          if(elementPos[y] >= scannerStart && elementPos[y] <= scannerEnd)
            elementCount[y] *= num;
        }
      }
      else if((chemical.charCodeAt(pointer-finder)>=65 && chemical.charCodeAt(pointer-finder)<=90) || (chemical.charCodeAt(pointer-finder)>=97 && chemical.charCodeAt(pointer-finder)<=122)) {
        var i = 0;
        if(chemical.charCodeAt(pointer-finder)>=65 && chemical.charCodeAt(pointer-finder)<=90)
          i = pointer-finder;
        else if(chemical.charCodeAt(pointer-finder)>=97 && chemical.charCodeAt(pointer-finder)<=122)
          i = pointer-finder-1;
        var pos = elementPos.indexOf(i);
        elementCount[pos] *= num;
      }
    }
    if(finder==0)
      pointer--;
    else
      pointer-=finder;
  }
  
  var molarMass = 0;
  for(var j=0;j<elementMass.length;j++)
    molarMass += (elementCount[j]*elementMass[j]);
  molarMass = parseFloat(molarMass).toFixed(2);
  
  $("#molarMass").html(molarMass);
  
  var nelementList = new Array();
  var nelementPos = new Array();
  var nelementCount = new Array();
  var nelementMass = new Array();
  for(var j=0;j<elementList.length;j++) {
    if(nelementList.indexOf(elementList[j]) == -1) {
      nelementList.push(elementList[j]);
      nelementPos.push(elementPos[j]);
      nelementCount.push(elementCount[j]);
      nelementMass.push(elementMass[j]);
    }
    else {
      nelementCount[nelementList.indexOf(elementList[j])] += elementCount[j];
    }
  }
  
  $("#molar-mass-table").html("<thead><th>Element</th><th>Average Atomic Mass</th><th>Atoms</th><th>Mass</th></thead><tbody>");
  for(var j=0;j<nelementList.length;j++) {
    $("#molar-mass-table").append("<tr><td class='t-element'>" + nelementList[j] + "</td><td class='t-mass'>" + nelementMass[j] + "</td><td class='t-count'>" + nelementCount[j] + "</td><td class='t-mult'>" + (nelementMass[j]*nelementCount[j]) + "</td></tr>");
  }
  $("#molar-mass-table").append("</tbody><tfoot><th></th><th></th><th></th><th>Molar Mass = " + molarMass + "</th></tfoot>");
}
  
getMolarMass();

});
